from Entidades.mineiro import Mineiro
from Validador.validador import Validador

class Dados:

    def __init__(self):
        self._dados = dict()
        self.identificador = 0

    def geradorIdentificador(self):
        self.identificador = self.identificador + 1
        return self.identificador
        
    def buscarPorIdentificador(self,paramIdentificador):
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            return self._dados.get(int(paramIdentificador))

    def buscarPorAtributo(self,param):
        lista = dict()        
        ultimoEncontrado = 0
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            for x in self._dados.values():
                if x.tamanho == param:
                    lista[x.identificador] = x
                    ultimoEncontrado = x.identificador
            if (len(lista) == 1):
                return lista[ultimoEncontrado]
            else:
                if (len(lista) == 0):
                    print("Nenhum encontrado")
                    return None
                return self.buscarPorIdentificadorComLista(lista)

    def buscarPorIdentificadorComlista(self,lista):
        print('Existem multiplos registros com o filtro informado: ')
        for x in lista.values():
            print(x)
        print('Informe um identificador: ')
        return lista.get(int(Validador.validar("\d+","Informe um valor do tipo inteiro:","")))

    def inserir(self,entidade):
        self._ultimoIdentificador = self.geradorIdentificador()
        entidade.identificador = self._ultimoIdentificador
        print(entidade)
        self._dados[self._ultimoIdentificador] = entidade


    def deletar(self,entidade):
        print(entidade)
        del self._dados[entidade.identificador]

    def alterar(self,entidade):
        self._dados[entidade.identificador] = entidade 