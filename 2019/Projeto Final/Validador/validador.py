import re

class Validador:

    @staticmethod
    def verificarInteiro():
        teste = False
        while teste == False:
            valor = input('\033[1;32mInforme um numero inteiro: \033[m')
            v = re.match("\d+",valor)
            if v != None:
                teste = True
        return int(valor)

    @staticmethod
    def validarOpcaoMenu(expReg):
        teste = False
        while teste == False:
            opcao = input('\033[1;32mInforme uma opcao: \033[m')
            v = re.match(expReg,opcao)
            if v != None:
                return opcao
            else:
                print('Opção invalida! Informe um valor entre {}'.format(expReg))
    @staticmethod
    def validarValorInformado(valorAtual,textoMsg):
        novoValor = input(textoMsg)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorAtual
        
    @staticmethod
    def validar(expReg,msgInvalido, msgValido):
        teste = False
        while teste == False:
            valor = input("Informe um valor:")
            verificarEx = re.match(expReg,valor)
            if verificarEx == None:
                print(msgInvalido.format(expReg))
            else:
                print(msgValido.format(valor))
                return valor

    @staticmethod
    def validarValorEntrada(valorAtual,msg):
        novoValor = input(msg)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorAtual

