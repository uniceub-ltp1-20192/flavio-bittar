#include <iostream>

using namespace std;

int quantidade(string msg);
void media(int quant);



//-----------------------------------------------------------------------
int main(){
//solicita a quantidade de notas
int quant = quantidade("Quantas notas voce deseja calcular? ");

//solicita as notas e seus pesos e da a media
media(quant);

	return 0;
}
//-----------------------------------------------------------------------



//funçoes
int quantidade(string msg){
	int quant;

	cout << msg;
	cin >> quant;
	return quant;
}

void media(int quant){
	float nota[quant], peso[quant], somaNota = 0, somaPeso = 0, somaNotaPeso = 0, media, mediaPeso;

	for(int i = 0; i < quant; i++){
		cout << "digite de [0 a 10] a " << i + 1 << "° nota: ";
		cin >> nota[i];

		cout << "digite o peso de [1 a 5] da " << i +1 << "° nota: ";
		cin >> peso[i];

		somaNota = somaNota + nota[i];
		somaPeso = somaPeso + peso[i];
		somaNotaPeso = nota[i] * peso[i] + somaNotaPeso;
	}

	media = somaNota/quant;
	mediaPeso = somaNotaPeso/somaPeso;

	cout << "A media das notas: " << media << "\n";
	cout << "A media ponderada das notas: " << mediaPeso << "\n";
}

