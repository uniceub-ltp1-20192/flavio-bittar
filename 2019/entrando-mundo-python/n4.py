#4.1
name = "       flavio nassim bittar      \n         joao pereira       "
print("original:",name,"\n")
print("-"*50)
print("corrigido[.lstrip()]:",name.lstrip(),"\n")
print("corrigido[.rstrip()]:",name.rstrip(),"\n")
print("corrigido[.strip()]:",name.strip(),"\n")
print("-"*50)
#4.2
phrase = input("digite uma frase: ")
phrase = phrase.strip().lower()

if " " in phrase:
	i = phrase.count(" ")
	numPhrase = len(phrase) - i
	newPhrase = phrase.split()
	print("\n"+"fresae juntada tem:",numPhrase,"caracteres","\n"+"frase juntada:","".join(newPhrase))
print("\n"+"fresae digitada tem:",len(phrase),"caracteres","\n"+"sua frase digitada:",phrase)
