#funçoes:
def transform(lists):
	'''funçao para transformar str em int'''
	term = 0
	while term < len(lists):
		lists[term] = int(lists[term])
		term += 1
	return lists

def makeLists(size):
	'''funçao para criaçao de uma lista'''
	lists = []
	print(f"\nsua lista tem: {size} termos")
	for term in range(size):
		lists.append(input(f"digite o {term + 1}° termo da lista: "))
	return lists

def cleanLists(lists):
	'''funçao para limpar a lista'''
	clean = 0
	while clean < len(lists):
		del lists[clean]
		clean += 1
		if clean == len(lists):
			clean = 0
	return lists

def doubleNumberLists(lists):
	''' funçao para dobrar os termos numericos de uma lista'''
	term = 0
	try:
		while term < len(lists):
			lists[term] *= 2
			term += 1
		return lists
	except(ValueError):
		print("sua lista nao e composta apenas por numeros !")

def comparative(lists1,lists2):
	'''funçao para comparar duas listas e mostrar termos em comum'''
	unusual = []
	for term in lists1:
		if term in lists2:
			unusual.append(term)
	return unusual

def sizeTermLists(lists):
	'''funçao para mostrar o tamanho de cada termo de uma lista'''
	size = []
	for term in lists:
		size.append(len(term.strip()))
	return size

def repeatError(msg,msgerror,types):
	'''funçao para verificaçao de erro'''
	verification = False
	while verification != True:
		try:
			var = types(input(msg))
			verification = True
			return var
		except(ValueError):
			verification = False
			print(msgerror)


#programa principal
#--------9.1-----------
print("faça uma lista numerica !")
amount = repeatError("digite a quantidade de termos em sua lista: ","o valor inserido nao e um numero !",int)
lists = makeLists(amount)
transform(lists)
print("lista criada: ",lists)
answer = input("digite S caso deseja apagar a lista: ")
if answer == "s" or answer == "S":
	cleanLists(lists)
	print("lista apagada: ",lists)
#--------9.2-----------
doubleNumberLists(lists)
print("\nlista com os numeros dobrados: ",lists)
#--------9.3-----------
try:
	print("O menor elemento da lista: ",min(lists),"\n")
except(ValueError):
	print("a lista esta vazia, logo nao existe o menor numero !\n")
#--------9.4-----------
print("iremos fazer um comparativo entre as listas...")
if answer == "s" or answer == "S":
	print("faça uma nova lista !")
	amount = repeatError("digite a quantidade de termos em sua lista: ","o valor inserido nao e um numero !",int)
	lists = makeLists(amount)
	print("\nfaça a lista a qual vai ser comparada !")
	amount = repeatError("digite a quantidade de termos em sua lista: ","o valor inserido nao e um numero !")
	newlists = makeLists(amount)
	common = comparative(lists,newlists)
	print("\nas listas tem em comum os termos :",common)
else:
	print("faça uma nova lista na qual sera comparada com a anterior !")
	amount = int(input("digite a quantidade de termos em sua lista: "))
	newlists = makeLists(amount)
	transform(newlists)
	common = comparative(lists,newlists)
	print("\nas listas tem em comum os termos :",common)
#--------9.5-----------
print("iremos ordenar sua lista !")
answer2 = int(input(f"selecione qual lista deseja utilizar [1- {lists} ou 2-{newlists}]: "))
if answer2 == 1 :
	lists.sort()
	print("lista em ordem",lists)
elif answer2 == 2:
	newlists.sort()
	print("lista em ordem",newlists)
else:
	print("voce nao selecionou nem uma das listas !")
	print("faça uma nova lista !")
	amount = repeatError("digite a quantidade de termos em sua lista: ","o valor inserido nao e um numero !",int)
	lists = makeLists(amount)
	lists.sort()
	print("lista em ordem",lists)
#--------9.6-----------
print("\niremos ver o tamanho de cada termo !")
print("faça uma nova lista !")
amount = repeatError("digite a quantidade de termos em sua lista: ","o valor inserido nao e um numero !",int)
lists = makeLists(amount)
size = sizeTermLists(lists)
print("\nO tamanho dos termos da sua lista sao: ",size)
