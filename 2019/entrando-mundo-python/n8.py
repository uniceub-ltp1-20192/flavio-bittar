#funçoes
def makeLists(size):
	lists = []
	for i in range(size):
		lists.append(input("Digite algo na lista: "))
	return lists

def transform(something):
	i = 0
	while i < len(something):
		something[i] = something[i].lower()
		i += 1
	return something

def listInvit():#7.1
	global listNames
	listNames = []
	stop = "S"
	while stop.lower() == "s":
		listNames.append(input("Digite o nome de quem voce deseja convidar para o jantar:"))
		print(" ")
		for x in listNames:
			print(x,end = ",")
		print(" ")
		stop = input("\ndigite [S] se deseja continuar:")

#programa principal
lists = makeLists(5)
transform(lists)
#-------------8.1-------------
print(" ")
print("original: ", lists)
print("ordem alfabetica: ", sorted(lists))
print("original novamente: ", lists)
print("ordem alfabetica inversa: ", sorted(lists, reverse = True))
print("original novamente: ", lists)
lists.reverse()
print("lista invertida: ", lists)
lists.reverse()
print("lista invertida novamente: ", lists)
lists.sort()
print("ordem alfabetica: ",lists)
lists.sort(reverse = True)
print("ordem alfabetica invertida: ",lists)
#-------------8.2-------------
listInvit()
print("voce chamou:",len(listNames),"convidados")
#-------------8.3-------------
num = int(input("digite o tamanho da sua lista: "))
lists = makeLists(num)
print("original: ", lists)
print("ordem alfabetica: ", sorted(lists))
print("original novamente: ", lists)
print("ordem alfabetica inversa: ", sorted(lists, reverse = True))
print("original novamente: ", lists)
lists.reverse()
print("lista invertida: ", lists)
lists.reverse()
print("lista invertida novamente: ", lists)
lists.sort()
print("ordem alfabetica: ",lists)
lists.sort(reverse = True)
print("ordem alfabetica invertida: ",lists)
