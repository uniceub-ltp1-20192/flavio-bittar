#------10.1--------
for i in range(1, 20 + 1):
	print (i)
#------10.2--------
num = []
for i in range(1, 1000000 + 1):
	num.append(i)
print (num)
#------10.3--------
num = []
for i in range(1, 1000000 + 1):
	num.append(i)
print (num)
print("O valor minimo da lista e: ",min(num))
print ("O valor maximo e: ",max(num))
#------10.4--------
numeros_impares = list(range(1,20,2))
print (numeros_impares)
#------10.5--------
mult = list(range(3,1000,3))
print (mult)
#------10.6--------
cubicos = []

for x in range(1,5):
	cubo = x**3
	cubicos.append(cubo)
print(cubicos)
