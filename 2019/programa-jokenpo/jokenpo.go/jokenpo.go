//diz quem é o pacote principal
package main

//importa bibliotecas
import("fmt";"math/rand";"time")

//funçao principal
func main(){
	user_name := askString("Nome: ")
	user_option := askString("Pedra, Papel ou Tesoura? ")

//valida o nome e a opçao
	if(!isNameValid(user_name) || !isOptionValid(user_option)){
		return
	}

//gera uma opçao aleatoria pela CPU
	cpu_option := generateRandomOption()

//mostra as opçoes escolhidas por ambos
	fmt.Printf("User: %v CPU: %v\n",user_option, cpu_option)

//pega o resultado
	result := getResult(user_option, cpu_option)

	printResult(result)
}

//funçao de perguntar
func askString(msg string) string {
	fmt.Print(msg)
	var input string
	fmt.Scanln(&input)
	return input
}

//funçao de validaçao de nome
func isNameValid(name string) bool{
	return true
}

//funçao de validaçao de opçao
func isOptionValid(option string) bool{
	return true
}

//gerador de opçoes aleatorio
func generateRandomOption() string{
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	number := r.Intn(3) + 1
		if (number == 1){
			return "Pedra"
		}

		if(number == 2){
			return "Papel"
		}
		return "Tesoura"
}

func getResult(ui string, ci string) string {
	if(ui == ci){
		return "Niguem ganhou"
	}

	if(ui == "Pedra" && ci == "Tesoura" || ui == "Tesoura" && ci == "Papel" || ui == "Papel" && ci == "Pedra"){
		return "Usuario"
	}

	return "CPU"
}

func printResult(result string) {
	fmt.Println("O vitorioso foi: " + result)
}
